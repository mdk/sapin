from io import BytesIO
from zipfile import ZipFile
import os

from flask import Flask, request

app = Flask(__name__)

expr_zip = BytesIO(
    # Look ma! An ugly binary blob! Is that how non-free software actually look?
    b"PK\x03\x04\x14\x00\t\x00\x08\x009\xb3\x90V\xc2U\xacy4\x00\x00\x001\x00"
    b"\x00\x00\x04\x00\x1c\x00exprUT\t\x00\x03\xceY<d\xceY<dux\x0b\x00\x01"
    b"\x04\xe8\x03\x00\x00\x04\xe8\x03\x00\x00\x1d'!d\x00kyl8 \x14g\x9dm\x16"
    b"\xa5\\\xc9\xd6%s\xb6M\xd2H\x8c'J,\tJ_\xe2m\x1e\xf5`\\j\x94gnJ\x8dw\x8e"
    b"L\x84fL*!PK\x07\x08\xc2U\xacy4\x00\x00\x001\x00\x00\x00PK\x01\x02\x1e\x03"
    b"\x14\x00\t\x00\x08\x009\xb3\x90V\xc2U\xacy4\x00\x00\x001\x00\x00\x00\x04"
    b"\x00\x18\x00\x00\x00\x00\x00\x01\x00\x00\x00\xa4\x81\x00\x00\x00\x00"
    b"exprUT\x05\x00\x03\xceY<dux\x0b\x00\x01\x04\xe8\x03\x00\x00\x04\xe8\x03"
    b"\x00\x00PK\x05\x06\x00\x00\x00\x00\x01\x00\x01\x00J\x00\x00\x00\x82\x00"
    b"\x00\x00\x00\x00"
)
with ZipFile(expr_zip) as myzip:
    with myzip.open("expr", pwd=os.environ["SAPIN_KEY"].encode()) as myfile:
        expr = myfile.read().decode()
        stage_width = lambda n: eval(expr, globals(), {"n": n})


def sapin(fir_width):
    lines = []
    draw_width = stage_width(fir_width) + fir_width * 2 + 4
    for stage in range(1, 1 + fir_width):
        current_width = stage_width(stage)
        for i in range(3 + stage):
            yield f"{'*' * (current_width + i * 2):^{draw_width}}"

    trunk_width = fir_width + 1 - fir_width % 2
    for i in range(fir_width):
        yield f"{'|' * trunk_width:^{draw_width}}"


@app.route("/")
def slash():
    try:
        fir_width = int(request.args.get("n"))
    except (TypeError, ValueError):
        return (
            "Give an `n` query string as an integer, like <a href='/?n=10'>/?n=10</a>."
        )
    if fir_width < 0 or fir_width > 100:
        return "Nah."
    return "<pre>" + "\n".join(sapin(fir_width)) + "</pre>"
